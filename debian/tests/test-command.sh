#!/bin/sh
export HOME="$AUTOPKGTEST_TMP"
unset http_proxy
unset https_proxy
unset no_proxy

# Run build-time tests
for py in $(py3versions -s 2> /dev/null)
do
  ${py} -m pytest -k \
    "not lcra and not cpc_drought and not usace_rivergages and not usgs_ned \
    and not cirs and not noaa_goes and not twc_kbdi and not usace_swtwc \
    and not usgs_nwis and not waterml_v1_1 and not wof \
    and not cdec_historical and not his_central \
    and not test_get_data and not test_get_station"
done

